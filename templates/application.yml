---
AWSTemplateFormatVersion: 2010-09-09

Description: >
  CloudFormation templates redunadant, secure and cost-efficient LAMP environment.
  Templates are deployed in the following sequence and build upon each other:
    network.yml - contains environment for 2 AZs deployment
      * VPC in ap-southeast-2
      * 2 private subnets in 2 AZs
      * 2 public subnets in 2 AZs
      * 3 Elastic IP - for Bastion and NAT Gateways
      * internet gateways
      * routing tables
      * NAT gateways
    security.yml - security configurations for the environment
      * security groups
      * 1 Bastion host accessible via elastic IP
    rds.yml - Provisioned RDS Aurora Mysql cluster with 2 nodes in private subnets
    application.yml - ASG for application instances (min size =2) with Application LB and bindings

Parameters:
  NetworkStackName:
    Type: String
    Description: Name of an active CloudFormation stack that contains the networking resources.
    ConstraintDescription: must be an alphanumeric string with hyphens up to 255 char long.
    MinLength: 1
    MaxLength: 255
    AllowedPattern: "^[a-zA-Z][-a-zA-Z0-9]*$"
    Default: "network"

  SecurityStackName:
    Type: String
    Description: Name of an active CloudFormation stack that contains the security resources.
    ConstraintDescription: must be an alphanumeric string with hyphens up to 255 char long.
    MinLength: 1
    MaxLength: 255
    AllowedPattern: "^[a-zA-Z][-a-zA-Z0-9]*$"
    Default: "security"

  KeyName:
    Type: 'AWS::EC2::KeyPair::KeyName'
    Description: Keyname of the ssh key for ec2-user account that will be used for all instances (EC2 keypair has to be created through AWS Management Console / CLI)
    ConstraintDescription: must be the name of an existing EC2 KeyPair.

  SSLCertificateArn:
    Type: String
    Description: Name of the SSL certificate that will be used to terminate HTTPS on ALB (Certificate has to be created in AWS Certificate Manager through AWS Management Console / CLI)
    #Default: arn:aws:acm:ap-southeast-2:xxxxxxxxxxxx:certificate/b549237c-ff24-45e4-947d-d3f773f00293

Mappings:
  # This allows to extend configuration to other regions and AMIs
  AWSRegionToAMI:
    ap-southeast-2:
      AMIamznlnx2: ami-0fb7513bcdc525c3b

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: "Nested stacks"
        Parameters:
          - NetworkStackName
          - SecurityStackName
      - Label:
          default: "Instance configurations"
        Parameters:
          - KeyName
      - Label:
          default: "ALB configurations"
        Parameters:
          - SSLCertificateArn

Resources:

  ApplicationLC:
    Type: AWS::AutoScaling::LaunchConfiguration
    Metadata: 
      AWS::CloudFormation::Init: 
        config: 
          packages:
            yum:
              httpd: []

          files: 
            /etc/cfn/cfn-hup.conf:
              content: !Sub |
                [main]
                stack=${AWS::StackId}
                region=${AWS::Region}
              mode: '000400'
              owner: root
              group: root
            /etc/cfn/hooks.d/cfn-auto-reloader.conf:
              content: !Sub |
                [cfn-auto-reloader-hook]
                triggers=post.update
                path=Resources.ApplicationLC.Metadata.AWS::CloudFormation::Init
                action=/opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource ApplicationLC --region ${AWS::Region}
                runas=root
              mode: '000400'
              owner: root
              group: root

          services: 
            sysvinit:
              httpd:
                enabled: 'true'
                ensureRunning: 'true'
              cfn-hup:
                enabled: 'true'
                ensureRunning: 'true'
                files: 
                - /etc/cfn/cfn-hup.conf
                - /etc/cfn/hooks.d/cfn-auto-reloader.conf
          #sources: 
          #  /var/www/html: "<public repo path>"
    Properties: 
      AssociatePublicIpAddress: 'false'
      BlockDeviceMappings: 
        - DeviceName: '/dev/xvda'
          Ebs: 
            VolumeSize: '10'
            VolumeType: 'gp2'
            DeleteOnTermination: 'true'
            Encrypted: 'true'
      ImageId: !FindInMap 
        - AWSRegionToAMI
        - !Ref 'AWS::Region'
        - AMIamznlnx2
      InstanceType: 't2.micro'
      KeyName: !Ref KeyName
      SecurityGroups: 
        - Fn::ImportValue: !Sub '${SecurityStackName}-ApplicationSG'
      UserData:
        Fn::Base64: 
          Fn::Sub: |
            #!/bin/bash -xe
            yum update -y aws-cfn-bootstrap
            /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource ApplicationLC --region ${AWS::Region}
            /opt/aws/bin/cfn-signal -e $? --stack ${AWS::StackName} --resource ApplicationASG --region ${AWS::Region}


  ApplicationLB:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Scheme: internet-facing
      Type: application
      Subnets:
        - Fn::ImportValue: !Sub '${NetworkStackName}-PublicSubnet1'
        - Fn::ImportValue: !Sub '${NetworkStackName}-PublicSubnet2'
      SecurityGroups:
        - Fn::ImportValue: !Sub '${SecurityStackName}-ALBSG'

  ALBHTTPTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 30
      UnhealthyThresholdCount: 10
      HealthCheckPath: /
      Port: 80
      Protocol: HTTP
      VpcId:
        Fn::ImportValue: !Sub '${NetworkStackName}-VPCID'

    # To-Do - implement sticky session / connection draining
    #https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-target-groups.html#target-group-attributes

  ALBHTTPSListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      Certificates:
        - CertificateArn: !Ref SSLCertificateArn
      DefaultActions:
        - Type: forward
          TargetGroupArn:
            Ref: ALBHTTPTargetGroup
      LoadBalancerArn:
        Ref: ApplicationLB
      Port: 443
      Protocol: HTTPS

  ApplicationASG:
    Type: AWS::AutoScaling::AutoScalingGroup
    CreationPolicy:
      ResourceSignal:
        Timeout: PT15M
        Count: '2'
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: '2'
        MaxBatchSize: '1'
        PauseTime: PT15M
        WaitOnResourceSignals: 'true'
    Properties: 
      LaunchConfigurationName: !Ref ApplicationLC
      VPCZoneIdentifier: 
        - Fn::ImportValue: !Sub '${NetworkStackName}-PrivateSubnet1'
        - Fn::ImportValue: !Sub '${NetworkStackName}-PrivateSubnet2'
      MinSize: '2'
      MaxSize: '20'
      DesiredCapacity: '2'
      MetricsCollection: 
        - 
          Granularity: "1Minute"
          Metrics: 
            - "GroupMinSize"
            - "GroupMaxSize"
      TargetGroupARNs:
        - !Ref ALBHTTPTargetGroup

  ApplicationASGScaleOutPolicy:
    Type: AWS::AutoScaling::ScalingPolicy
    Properties:
      AutoScalingGroupName: !Ref ApplicationASG
      AdjustmentType: "ChangeInCapacity"
      PolicyType: "SimpleScaling"
      Cooldown: "60"
      AutoScalingGroupName: !Ref ApplicationASG
      ScalingAdjustment: 1
  ApplicationASGScaleOutAlarm:
    Type: 'AWS::CloudWatch::Alarm'
    Properties:
      EvaluationPeriods: '1'
      Statistic: Average
      Threshold: '70'
      AlarmDescription: Scale up alarm when CPU Utilization > 70% for 1 minute
      Period: '60'
      AlarmActions:
      - !Ref ApplicationASGScaleOutPolicy
      Namespace: AWS/EC2
      Dimensions:
      - Name: AutoScalingGroupName
        Value: !Ref ApplicationASG
      ComparisonOperator: GreaterThanThreshold
      MetricName: CPUUtilization
    
  ApplicationASGScaleInPolicy:
    Type: AWS::AutoScaling::ScalingPolicy
    Properties:
      AutoScalingGroupName: !Ref ApplicationASG
      AdjustmentType: "ChangeInCapacity"
      PolicyType: "SimpleScaling"
      Cooldown: "60"
      AutoScalingGroupName: !Ref ApplicationASG
      ScalingAdjustment: -1
  ApplicationASGScaleInAlarm:
    Type: 'AWS::CloudWatch::Alarm'
    Properties:
      EvaluationPeriods: '2'
      Statistic: Average
      Threshold: '30'
      AlarmDescription: Scale up alarm when CPU Utilization > 70% for 1 minute
      Period: '300'
      AlarmActions:
      - !Ref ApplicationASGScaleInPolicy
      Namespace: AWS/EC2
      Dimensions:
      - Name: AutoScalingGroupName
        Value: !Ref ApplicationASG
      ComparisonOperator: LessThanThreshold
      MetricName: CPUUtilization      


Outputs:

  ALBDNS: 
    Value: !GetAtt ApplicationLB.DNSName
