```git clone git@bitbucket.org:vladsteam/cfn.git```

# Challenge
Provide a Cloud formation template defining a redundant, secure and cost effective LAMP stack within AWS.

# Interpretations
The following interpretations have been made based on challange, anticipated effort (few days) and purpose of the project, as  well as based on technologies mentioned in the Job Description.
Those were stated as assumptions and later clarified and confirmed by Kurt.

### Template format
Cloud Formation deployment can be via a single template or via multiple templates. Multiple templates allow to progressively deploy environment (Networking, Security Stack, Application Instances), enabling to minimize "blast radius" in case of problems with deployment.

### Interpretations of "Redundant"
* Highly-vailable (attemting to remain operational with any component failure) as opposed to resilient (ability to self-heal eventually)
* Resiliency within a single AWS region / multiple availability zones is sufficient

### Interpretations of "Secure"
* Fully trusting inbuilt security of AWS services (including confidentiality of AWS software-defined network, KMS, AWS Certificate Manager, integrity and security of Amazon Linux AMIs)
* At-rest data encryption is required
* In-transit data encryption for communications outside of VPC is required
* Intra-VPC communication is considered secure, so in-transit encryption within VPC is not required
* Communication between instances is restricted and only required ports are open
* Minimize reliance on hardcoded secrets
* Internet access is available from all instances
* Management access to Linux instances is via Bastion host

### Interpretations of "Cost effective"
* Auto-scaling as opposed to over-provisioning to sustain the peak load


# Exclusions
Taking the nature of this project, certain practices and technologies that are prudent in mission-critical WEB-based production environments are excluded from the scope.

### Advanced AWS services 
Intergation of advanced AWS services will be outside of the scope.

* CloudWatch
* CloudTrail
* AWS Organization with multiple IAM accounts for permission separation and blast radius minimization
* Identity management services
* AWS Shield / WAF

### Hardening of Linux
Amazon Linux AMIs will not be customized and no additional security controls will be added.
Management access to all environment instances will be via bastion host via ec2-user's sshkey (pre-created via console or awscli).

_Note: while application instances will not require SSH access for initial setup and day to day operations, from operational standpoint this will be eventually required for troubleshooting, and for secure architecture this consideration is essential._

### CI/CD pipeline
Deployment of the environment will be done from AWS CloudFormation template in AWS Console or from AWS CLI.

No provisioning from CI/CD pipeline will me made.

### Testing
No provisioning for infrastructure or application testing will be made.


# Architecture Diagram
![Architecture Diagram](architecture_diagram.png)

# Solution Description

|   Parameter  |    Value   |
|--------------|------------|
|Region |    ap-southeast-2 (Sydney) |
|AMIs |     Amazon Linux 2 (systemd) AMIs |
|Application instance types | t2.micro|
|RDS instance types | db.t2.small |
|Bastion instance type | t2.nano|

Solution is designed in 3 layer model that include presentation, logic and data layers.
Solution has its own VPC. 4 subnets defined in 2 availability zones (AZs) within the region.

Presentation layer is represented by internet-facing Application Load Balancer (ALB) that uses AWS Certificate Manager certificate (deployed via console) to terminate HTTPS. ALB is configured to forward HTTP (unencrypted) traffic to logic (middleware) layer. ALB is configured to monitor target group instances and use sticky sessions as stateful application is assumed. Incoming connections from ALB to logic layer is restricted by security group only allowing 80/tcp (http) from ALB.

Logic layer is represented by an Auto Scaling Group (ASG) with minimal number of instances equal 2 which is configured to scale out and in based on CloudWatch metric of CPU load (scale out trigger - CPU load >= 70% for 2 consequitive 1 minute periods, scale in trigger - CPU load <=20% for 2 consequitive 1 minute periods).
Scaling policy - 50/50 between AZ 1 and 2 - which ensures fault tolerance by locating at least one active instance per AZ.
ASG is conifgured to receive traffic from ALB via a target group.
Application instances deployed by the ASG have access to internet and is application components are deployed by user-data scripts via yum and git.

Data layer is represented by Amazon RDS Postgres active-standby instances that ensures fault tolerance in a cost-effective manner. Due to scalability limitation of RDBMS, it has to be overprovisioned for maximum anticipated load. Complex solutions like RDMBS partioning/SQL Proxy are outside of scope of this project.

Management infrastructure is represented by a Bastion host - a Linux host that allows incoming SSH connections from public internet  (possibly restricted by security group) to its associated elastic IP and ability to ssh to application instances.

At-rest data protection is leveraged via EBS-encrypted volumes of RDS and EC2 instances. Key management is handled by KMS. Encryption key management is handled by AWS-managed CMKs (aws/ebs and aws/rds).

Environment is deployed via 4 CloudFormation stacks from 4 interdependent templates (in the following sequence):

1. templates/network.yml - VPC and network resources

2. templates/security.yml - Security and management resources (Security groups, Bastion host)

3. templates/rds.yml - RDS Aurora Cluster Mysql 5.6.10

4. templates/application.yml - Launch configuration for application instances with encrypted root EBS volumes, ALB, target group, ASG, CloudWatch Alarms and Scaling In/Out Policies for ASG


The following resources are created manually via console /cli and their ARNs are provided as input variables for CloudFormation stacks:

* EC2 instance sshkeys

* SSL certificate imported into AWS Certificate Manager (Self-signed certificate files are available in misc/testcert.me.*)

Output of the application stack is FQDN of ALB which can be used to access the application.

Application deployment can be embedded in cloud-init metadata of the launch configuration in templates/application.yaml


# Timelines
Estimated effort: 8-10 hours

Estimated velocity: 2-3 hours per day

Estimated completion: Tuesday 18/06
